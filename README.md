# ActionScript-3
Funções para banner em Flash no Google Adwords

**Passo a passo**

Adicione a função abaixo no primeiro frame

```
import flash.display.LoaderInfo;
import flash.events.MouseEvent;
import flash.net.URLRequest;
 
btnClickTag.addEventListener(MouseEvent.CLICK, function(event: MouseEvent):void {
    flash.net.navigateToURL(new URLRequest(root.loaderInfo.parameters.clickTAG), "_blank");
});
```

Crie um botão transparente e coloque o nome de **btnClickTag**
